**************************
Theme: A Sync Template 2.0
Author: FiNeX
Website: www.finex.org
**************************

A Sync Template 2.0 is an HTML5 Drupal 7 theme with 16 flexible regions!

Project site: http://drupal.org/project/async

Thanks to Framework theme 3.0 by André Griffin



Powered by
- http://www.themes-drupal.org
- http://www.siti-drupal.it
- http://www.realizzazione-siti-vicenza.com